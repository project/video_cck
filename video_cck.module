<?php

define('VIDEO_CCK_DEFAULT_VIDEO_WIDTH', 425);
define('VIDEO_CCK_DEFAULT_VIDEO_HEIGHT', 350);
define('VIDEO_CCK_DEFAULT_PREVIEW_WIDTH', 425);
define('VIDEO_CCK_DEFAULT_PREVIEW_HEIGHT', 350);
define('VIDEO_CCK_DEFAULT_THUMBNAIL_WIDTH', 120);
define('VIDEO_CCK_DEFAULT_THUMBNAIL_HEIGHT', 90);

/**
 * Implement hook_menu
 */
function video_cck_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/content/video_cck',
      'title' => t('Video CCK configuration'),
      'description' => t('Configure Video CCK: Allow content types to use various 3rd party providers, enter API keys, etc.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'video_cck_settings',
      'access' => user_access('administer site configuration'));
  }
  return $items;
}

/**
 * Implement hook_settings
 */
function video_cck_settings() {
  $form = array();
  $providers = video_cck_system_list();
  $header = array(t('Feature'), t('Supported'), t('Notes'));
  foreach ($providers as $provider) {
    $info = video_cck_include_invoke($provider->name, 'info');
    $form[$provider->name] = array(
      '#type' => 'fieldset',
      '#title' => t('@provider configuration', array('@provider' => $info['name'])),
      '#description' => $info['settings_description'],
      '#collapsible' => true,
      '#collapsed' => true,
    );
    if (is_array($info['supported_features']) && !empty($info['supported_features'])) {
      $form[$provider->name]['supported_features'] = array(
        '#type' => 'fieldset',
        '#title' => t('Supported features'),
        '#description' => t('This is a list of the current state of support for the following features by %provider:', array('%provider' => $info['name'])),
        '#collapsible' => true,
        '#collapsed' => true,
        '#weight' => 7,
      );
      $form[$provider->name]['supported_features']['features'] = array(
        '#type' => 'markup',
        '#value' => theme('table', $header, $info['supported_features']),
      );
    }
    $form[$provider->name]['video_cck_allow_' . $provider->name] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow videos from %provider', array('%provider' => $info['name'])),
      '#description' => t('If checked, then content types may be created that allow video to be provided by %provider.', array('%provider' => $info['name'])),
      '#weight' => -10,
      '#default_value' => variable_get('video_cck_allow_' . $provider->name, true),
    );
    $form[$provider->name][] = video_cck_include_invoke($provider->name, 'settings');
  }
  return system_settings_form($form);
}

/**Implementation of hook_field_info  **/

function video_cck_field_info() {
  $fields = array(
    'video_cck' => array('label' => t('Embedded Video')),
  );
  return $fields;
}

/** Implementation of hook_field_settings **/

function video_cck_field_settings($op, $field) {
  switch ($op) {

    case 'database columns':
      $columns = array(
        'embed' => array('type' => 'longtext', 'not null' => TRUE, 'default' => "''", 'sortable' => TRUE),
        'value' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => "''", 'sortable' => TRUE),
        'provider' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => "''", 'sortable' => TRUE),
        'data' => array('type' => 'longtext', 'not null' => TRUE, 'default' => "''", 'sortable' => false),
      );
      switch ($field['type']) {
        case 'video_cck':
          break;
      }
      return $columns;
  }
}

/** Implementation of hook_field **/

function video_cck_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {

  // TODO: nothing to validate at the moment. we need to have different api's have a chance to validate
  case 'validate':
    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $error_field = $field['field_name'].']['.$delta.'][embed';

        _video_cck_field_validate_id($field, $item, $error_field);
      }
    }
    else {
      $error_field = $field['field_name'];

      _video_cck_field_validate_id($field, $items[0], $error_field);
    }
    break;

  case 'submit':
    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $list = _video_cck_field_submit_id($field, $item);
        $items[$delta]['provider'] = $list['provider'];
        $items[$delta]['value'] = $list['value'];
        $items[$delta]['data'] = $list['data'];
      }
    }
    else {
      $list = _video_cck_field_submit_id($field, $items[0]);
      $items[0]['provider'] = $list['provider'];
      $items[0]['value'] = $list['value'];
      $items[0]['data'] = $list['data'];
    }
    break;

  case 'insert':
  case 'update':
    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $items[$delta]['data'] = serialize($items[$delta]['data']);
      }
    }
    else {
      $items[0]['data'] = serialize($items[0]['data']);
    }
    break;

  case 'load':
    $field_name = $field['field_name'];

    if ($field['multiple']) {
      foreach ($items as $delta => $item) {
        $data = (array)unserialize($items[$delta]['data']);
        $items[$delta]['data'] = $data;
        $node->{$field_name}[$delta]['data'] = $data;
      }
    }
    else {
      $data = (array)unserialize($items[0]['data']);
      $items[0]['data'] = $data;
      $node->{$field_name}[0]['data'] = $data;
    }
    $return = array();
    $return[$field_name] = $items;
    return $return;
    break;
  }
}

/**
 * return a list of providers allowed for a specific field
 */
function video_cck_allowed_providers($field) {
  $allowed_providers = video_cck_system_list();
  $providers = array();
  $allow_all = true;
  foreach ($allowed_providers as $test) {
    if (!variable_get('video_cck_allow_' . $test->name, true)) {
      unset($allowed_providers[$test->name]);
    }
    else {
      $allow_all &= !$field['widget']['providers'][$test->name];
    }
  }
  if (!$allow_all) {
    foreach ($allowed_providers as $test) {
      if (!$field['widget']['providers'][$test->name]) {
        unset($allowed_providers[$test->name]);
      }
    }
  }
  return $allowed_providers;
}

/**
 *  This will parse the url or embedded code pasted by the node submitter.
 *  returns either an empty array (if no match), or an array of provider and value.
 */
function video_cck_parse_embed($field, $embed = '') {
  if ($embed) {
    $providers = video_cck_allowed_providers($field);
    foreach ($providers as $provider) {
      $success = video_cck_include_invoke($provider->name, 'extract', $embed);
      // we've been given an array of regex strings, so let's see if we can find a match
      if (is_array($success)) {
        foreach ($success as $regex) {
          $matches = NULL;
          if (preg_match($regex, $embed, $matches)) {
            return array('provider' => $provider->name, 'value' => $matches[1]);
          }
        }
      }
      // the invoked include module did its own parsing and found a match
      else if ($success) {
        return array('provider' => $provider->name, 'value' => $success);
      }
    }
  }
  // we found no match
  return array();
}

/**
 *  extract the video id from embedded code or video url
 */
function _video_cck_field_validate_id($field, $item, $error_field) {
  // does nothing at this time... TODO: fix this up
  return _video_cck_field_submit_id($field, $item);
}

/**
 *  replace embedded code with the extracted id. this goes in the field 'value'
 *  also allows you to grab directly from the URL to play the video from field 'provider'
 */
function _video_cck_field_submit_id($field, $item) {
  $item = array_merge($item, video_cck_parse_embed($field, $item['embed']));
  $item['data'] = (array)video_cck_include_invoke($item['provider'], 'data', $field, $item);
  return $item;
}

/** Implementation of hook_field_formatter_info **/
function video_cck_field_formatter_info() {
  $types = array('video_cck',);
  $formats = array(
    'default' => array(
      'label' => t('Default'),
      'field types' => $types,
    ),
    'video_video' => array(
      'label' => t('Full Size Video'),
      'field types' => $types,
    ),
    'video_preview' => array(
      'label' => t('Preview Video'),
      'field types' => $types,
    ),
    'video_thumbnail' => array(
      'label' => t('Image Thumbnail'),
      'field types' => $types,
    ),
    'video_embed' => array(
      'label' => t('Embed Code'),
      'field types' => $types,
    ),
  );
  return $formats;
}

/** Implementation of hook_field_formatter **/

function video_cck_field_formatter($field, $item, $formatter, $node) {
  // if we're coming from a preview, we need to extract our new embedded value...
  if ($item['in_preview']) {
    $item = video_cck_parse_embed($field, $item['embed']);
  }

  // if we have no value, then return an empty string
  if (!isset($item['value'])) {
    return '';
  }

  // unfortunately, when we come from a view, we don't get all the widget fields
  if (!$node->type) {
    $type = content_types($field['type_name']);
    $field['widget'] = $type['fields'][$field['field_name']]['widget'];
  }

  // and sometimes our data is still unserialized, again from views
  if (!is_array($item['data'])) {
    $item['data'] = (array)unserialize($item['data']);
  }

  switch ($formatter) {
    case 'video_thumbnail':
      $output .= theme('video_cck_thumbnail', $field, $item, $formatter, $node);
      break;
    case 'video_preview':
      $output .= theme('video_cck_preview', $field, $item, $formatter, $node);
      break;
    case 'video_embed':
      $output .= theme('video_cck_embed', $field, $item, $formatter, $node);
      break;
    case 'video_video':
    case 'default':
    default:
      $output .= theme('video_cck_video', $field, $item, $formatter, $node);
      break;
  }
  return $output;
}

/** Widgets **/

/** Implementation of hook_widget_info **/
function video_cck_widget_info() {
  return array(
    'video_cck_textfields' => array(
      'label' => t('3rd Party Video'),
      'field types' => array('video_cck',),
    ),
  );
}

function video_cck_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      if ($widget['type'] == 'video_cck_textfields') {
        // retrieve list of allowed providers
        $options = array();
        $providers = video_cck_system_list();
        foreach ($providers as $provider) {
          if (variable_get('video_cck_allow_' . $provider->name, true)) {
            $info = video_cck_include_invoke($provider->name, 'info');
            $options[$provider->name] = $info['name'];
          }
        }
        $form['provider_list'] = array(
          '#type' => 'fieldset',
          '#title' => t('Video Providers Supported'),
          '#description' => t('Select which third party video providers you wish to allow for this content type from the list below. If no checkboxes are checked, then all providers will be supported. When a user submits new content, the URL they enter will be matched to the provider, assuming that provider is allowed here.'),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['provider_list']['providers'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Providers'),
          '#default_value' => $widget['providers'] ? $widget['providers'] : array(),
          '#options' => $options,
        );

        $width = variable_get('video_cck_default_video_width', VIDEO_CCK_DEFAULT_VIDEO_WIDTH);
        $height = variable_get('video_cck_default_video_height', VIDEO_CCK_DEFAULT_VIDEO_HEIGHT);
        $form['video'] = array(
          '#type' => 'fieldset',
          '#title' => t('Video Display Settings'),
          '#description' => t('These settings control how this video is displayed in its full size, which defaults to @widthx@height.', array('@width' => $width, '@height' => $height)),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['video']['video_width'] = array(
          '#type' => 'textfield',
          '#title' => t('Video display width'),
          '#default_value' => $widget['video_width'] ? $widget['video_width'] : $width,
          '#required' => true,
          '#description' => t('The width of the video. It defaults to @width.', array('@width' => $width)),
        );
        $form['video']['video_height'] = array(
          '#type' => 'textfield',
          '#title' => t('Video display height'),
          '#default_value' => $widget['video_height'] ? $widget['video_height'] : $height,
          '#required' => true,
          '#description' => t('The height of the video. It defaults to @height.', array('@height' => $height)),
        );
        $form['video']['video_autoplay'] = array(
          '#type' => 'checkbox',
          '#title' => t('Autoplay'),
          '#default_value' => $widget['video_autoplay'] ? $widget['video_autoplay'] : false,
          '#description' => t('If supported by the provider, checking this box will cause the video to automatically begin after the video loads when in its full size.'),
        );

        $width = variable_get('video_cck_default_preview_width', VIDEO_CCK_DEFAULT_PREVIEW_WIDTH);
        $height = variable_get('video_cck_default_preview_height', VIDEO_CCK_DEFAULT_PREVIEW_HEIGHT);
        $form['preview'] = array(
          '#type' => 'fieldset',
          '#title' => t('Video Preview Settings'),
          '#description' => t('These settings control how this video is displayed in its preview size, which defaults to @widthx@height.', array('@width' => $width, '@height' => $height)),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['preview']['preview_width'] = array(
          '#type' => 'textfield',
          '#title' => t('Video preview width'),
          '#default_value' => $widget['preview_width'] ? $widget['preview_width'] : variable_get('video_cck_default_preview_width', VIDEO_CCK_DEFAULT_PREVIEW_WIDTH),
          '#required' => true,
          '#description' => t('The width of the preview video. It defaults to @width.', array('@width' => $width)),
        );
        $form['preview']['preview_height'] = array(
          '#type' => 'textfield',
          '#title' => t('Video preview height'),
          '#default_value' => $widget['preview_height'] ? $widget['preview_height'] : variable_get('video_cck_default_preview_height', VIDEO_CCK_DEFAULT_PREVIEW_HEIGHT),
          '#required' => true,
          '#description' => t('The height of the preview video. It defaults to @height.', array('@height' => $height)),
        );
        $form['preview']['preview_autoplay'] = array(
          '#type' => 'checkbox',
          '#title' => t('Autoplay'),
          '#default_value' => $widget['preview_autoplay'] ? $widget['preview_autoplay'] : false,
          '#description' => t('If supported by the provider, checking this box will cause the video to automatically begin after the video loads when in its preview size.'),
        );

        $width = variable_get('video_cck_default_thumbnail_width', VIDEO_CCK_DEFAULT_THUMBNAIL_WIDTH);
        $height = variable_get('video_cck_default_thumbnail_height', VIDEO_CCK_DEFAULT_THUMBNAIL_HEIGHT);
        $form['tn'] = array(
          '#type' => 'fieldset',
          '#title' => t('Thumbnail'),
          '#description' => t('When displayed as a thumbnail, these settings control the image returned. Note that not all 3rd party video content providers offer thumbnails, and others may require an API key or other requirements. More information from the !settings. The default size for thumbnails is @widthx@height.', array('!settings' => l(t('settings page'), 'admin/content/video_cck'), '@width' => $width, '@height' => $height)),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['tn']['thumbnail_width'] = array(
          '#type' => 'textfield',
          '#title' => t('Video width'),
          '#default_value' => $widget['thumbnail_width'] ? $widget['thumbnail_width'] : variable_get('video_cck_default_thumbnail_width', VIDEO_CCK_DEFAULT_THUMBNAIL_WIDTH),
          '#required' => true,
          '#description' => t('The width of the thumbnail. It defaults to @width.', array('@width' => $width)),
        );
        $form['tn']['thumbnail_height'] = array(
          '#type' => 'textfield',
          '#title' => t('Thumbnail height'),
          '#default_value' => $widget['thumbnail_height'] ? $widget['thumbnail_height'] : variable_get('video_cck_default_thumbnail_height', VIDEO_CCK_DEFAULT_THUMBNAIL_HEIGHT),
          '#required' => true,
          '#description' => t('The height of the thumbnail. It defaults to @height.', array('@height' => $height)),
        );
      }
      return $form;

    case 'validate':
      if ($widget['type'] == 'video_cck_textfields') {
        if (!is_numeric($widget['video_width']) || intval($widget['video_width']) != $widget['video_width'] || $widget['video_width'] < 1) {
          form_set_error('video_width', t('"Video width" must be a positive integer.'));
        }
        if (!is_numeric($widget['video_height']) || intval($widget['video_height']) != $widget['video_height'] || $widget['video_height'] < 1) {
          form_set_error('video_height', t('"Video height" must be a positive integer.'));
        }
        if (!is_numeric($widget['preview_width']) || intval($widget['preview_width']) != $widget['preview_width'] || $widget['preview_width'] < 1) {
          form_set_error('preview_width', t('"Preview width" must be a positive integer.'));
        }
        if (!is_numeric($widget['preview_height']) || intval($widget['preview_height']) != $widget['preview_height'] || $widget['preview_height'] < 1) {
          form_set_error('preview_height', t('"Preview height" must be a positive integer.'));
        }
        if (!is_numeric($widget['thumbnail_width']) || intval($widget['thumbnail_width']) != $widget['thumbnail_width'] || $widget['thumbnail_width'] < 1) {
          form_set_error('thumbnail_width', t('"Thumbnail width" must be a positive integer.'));
        }
        if (!is_numeric($widget['thumbnail_height']) || intval($widget['thumbnail_height']) != $widget['thumbnail_height'] || $widget['thumbnail_height'] < 1) {
          form_set_error('thumbnail_height', t('"Thumbnail height" must be a positive integer.'));
        }
      }
      break;

    case 'save':
      if ($widget['widget']['type'] == 'video_cck_textfields') {
        return array('video_width', 'video_height', 'video_autoplay', 'preview_width', 'preview_height', 'preview_autoplay', 'thumbnail_width', 'thumbnail_height', 'providers', );
      }
      break;
  }
}

/** Implementation of hook_widget **/

function video_cck_widget($op, &$node, $field, &$node_field) {
  switch ($op) {
    case 'form':
      $form = array();

      $form[$field['field_name']] = array('#tree' => TRUE);
      $textfield = 'embed';
      $field['required'] = FALSE;
      $providers = video_cck_allowed_providers($field);
      $urls = array();
      foreach ($providers as $provider) {
        // don't check providers not allowed
        if (variable_get('video_cck_allow_' . $provider->name, true)) {
          $info = video_cck_include_invoke($provider->name, 'info');
          $urls[] = l($info['name'], $info['url'], array('target' => '_blank'));
        }
      }
      $textfield_title = t($field['widget']['label']);
      $textfield_description = t('Enter the Video\'s URL or Embed Code here. For instance, you might enter the URL from the address bar of a video at Google Videos, or the code from the Embed bar provided by YouTube. The video will be parsed and displayed appropriately from this.');
      $textfield_description .= '<br />' . t('The following services are provided: !urls', array('!urls' => implode(', ', $urls)));

      if ($field['multiple']) {
        $form[$field['field_name']]['#type'] = 'fieldset';
        $form[$field['field_name']]['#title'] = t($field['widget']['label']);
        $delta = 0;
        foreach ($node_field as $data) {
          if (isset($data[$textfield])) {
            $form[$field['field_name']][$delta][$textfield] = array(
              '#type' => 'textfield',
              '#title' => $textfield_title,
              '#description' => $textfield_description,
              '#default_value' => $data[$textfield],
              '#required' => ($delta == 0) ? $field['required'] : FALSE,
              '#maxlength' => 1024,
            );
            $form[$field['field_name']][$delta]['value'] = array(
              '#type' => 'value',
              '#value' => $data['value'],
            );
            if ($data['value']) {
              $info = video_cck_include_invoke($data['provider'], 'info');
              $form[$field['field_name']][$delta]['markup_value'] = array(
                '#type' => 'item',
                '#value' => t('(@provider Video ID: !value)', array('@provider' => $info['name'], '!value' => l($data['value'], video_cck_include_invoke($info['provider'], 'video_link', $data['value']), array('target' => '_blank')))),
              );
            }
            $delta++;
          }
        }
        foreach (range($delta, $delta + 2) as $delta) {
           $form[$field['field_name']][$delta][$textfield] = array(
            '#type' => 'textfield',
            '#title' => $textfield_title,
            '#description' => $textfield_description,
            '#default_value' => '',
            '#required' => ($delta == 0) ? $field['required'] : FALSE,
            '#maxlength' => 1024,
          );
          $form[$field['field_name']][$delta]['value'] = array(
            '#type' => 'value',
            '#title' => '',
          );
        }
      }
      else {
        $form[$field['field_name']][0][$textfield] = array(
          '#type' => 'textfield',
          '#title' => $textfield_title, //t($field['widget']['label']),
          '#description' => $textfield_description,
          '#default_value' => isset($node_field[0][$textfield]) ? $node_field[0][$textfield] : '',
          '#required' => $field['required'],
          '#maxlength' => 1024,
        );
        if ($textfield == 'embed') {
          $value = isset($node_field[0]['value']) ? $node_field[0]['value'] : '';
          $form[$field['field_name']][0]['value'] = array(
            '#type' => 'value',
            '#value' => $value,
          );
          if ($value) {
            $info = video_cck_include_invoke($node_field[0]['provider'], 'info');
            $form[$field['field_name']][0]['value_markup'] = array(
              '#type' => 'item',
              '#value' => t('(@provider Video ID: !value)', array('@provider' => $info['provider'], '!value' => l($value, video_cck_include_invoke($info['provider'], 'video_link', $value), array('target' => '_blank')))),
            );
          }
        }
      }
      return $form;
    default:
      break;
  }
}

/**
 * When an include file requires to read an xml to receive information, such as for thumbnails,
 * this script can be used to request the xml and return it as an array.
 * Note that this is a modified function from the flickr.module, made to handle this type of
 * call more generically. also, i suspect it could be done easier (and more quickly) in php 5.
 *   @param $provider
 *     the string of the third party provider, such as 'youtube' or 'google'
 *   @param $url
 *     the url for the xml request
 *   @param $args
 *     an array of args to pass to the xml url
 *   @param $cacheable
 *     optional; if true, the result of this xml request will be cached. good to play nice w/
 *     the third party folks so they don't stop providing service to your site...
 *   @return
 *     the xml results returned as an array
 */
function video_cck_request_xml($provider, $url, $args = array(), $cacheable = true) {
  ksort($args);

  // build an argument hash that we'll use for the cache id and api signing
  $arghash = $provider . ':';
  foreach($args as $k => $v){
    $arghash .= $k . $v;
  }

  // build the url
  foreach ($args as $k => $v){
    $encoded_params[] = urlencode($k).'='.urlencode($v);
  }
  if (!empty($encoded_params)) {
    $url .= '?'. implode('&', $encoded_params);
  }

  // some providers, such as bliptv, actually change the url, and not just the queries.
  $arghash .= $url;

  // if it's a cachable request, try to load a cached value
  if ($cacheable) {
    if ($cache = cache_get($arghash, 'cache')) {
      return unserialize($cache->data);
    }
  }

  // connect and fetch a value
  $result = drupal_http_request($url);

  if ($result->code == 200) {
    $parser = drupal_xml_parser_create($result->data);
    $vals = array();
    $index = array();
    xml_parse_into_struct($parser, $result->data, $vals, $index);
    xml_parser_free($parser);

    $params = array();
    $level = array();
    $start_level = 1;
    foreach ($vals as $xml_elem) {
      if ($xml_elem['type'] == 'open') {
        if (array_key_exists('attributes',$xml_elem)) {
          list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
        } else {
          $level[$xml_elem['level']] = $xml_elem['tag'];
        }
      }
      if ($xml_elem['type'] == 'complete') {
        $php_stmt = '$params';
        while($start_level < $xml_elem['level']) {
          $php_stmt .= '[$level['.$start_level.']]';
          $start_level++;
        }
        $php_stmt .= '[$xml_elem[\'tag\']][] = $xml_elem[\'value\'];' . $php_stmt . '[$xml_elem[\'tag\']][] = $xml_elem[\'attributes\'];';
        eval($php_stmt);
        $start_level--;
      }
    }

    // save a cacheable result for future use
    if ($cacheable) {
      cache_set($arghash, 'cache', serialize($params), time() + 3600);
    }
    return $params;
  }
  return array();
}

/**
 * Return an array of installed .inc files and/or loads them upon request.
 * This routine is modeled after drupal_system_listing() (and also depends on it).
 * It's major difference, however, is that it loads .inc files by default.
 *
 * @param $provider
 *   Optional; name of the passed $provider to find (e.g. "youtube", "google", etc.).
 * @param $load
 *   Defaults to true; whether to include matching files into memory.
 * @return
 *   An array of file objects optionally matching $provider.
 */
function video_cck_system_list($provider = NULL, $load = true) {
  $files = drupal_system_listing("$provider\.inc", drupal_get_path('module', 'video_cck')."/providers", 'name', 0);

  ksort($files);

  if ($load) {
    foreach ($files as $file) {
      video_cck_include_list($file);
    }
  }

  return $files;
}

/**
 * Maintains a list of all loaded include files.
 *
 * @param $file
 *   Optional; a file object (from video_cck_system_list()) to be included.
 * @return
 *   An array of all loaded includes (without the .inc extension).
 */
function video_cck_include_list($file = NULL) {
  static $list = array();

//  if (!$list) { $list = array(); }

  if ($file && !isset($list[$file->name])) {
    include_once('./'.$file->filename);
    $list[$file->name] = $file->name;
  }

  return $list;
}

/**
 * Determine whether an include implements a hook, cf. module_hook.
 *
 * @param $provider
 *   The name of the provider file (without the .inc extension), such as 'youtube' or 'google'.
 * @param $hook
 *   The name of the hook (e.g. "thumbnail", "settings", etc.).
 * @return
 *   TRUE if the provider is loaded and the hook is implemented.
 */
function video_cck_include_hook($provider, $hook) {
  return function_exists('video_cck_' . $provider .'_'. $hook);
}

/**
 * Invoke hook in a particular include. If the include does not implement
 * the hook, a default value is returned.
 *
 * @param $provider
 *   The name of the provider (without the .inc extension).
 * @param $hook
 *   The name of the hook (e.g. "settings", "thumbnail", etc.).
 * @param ...
 *   Arguments to pass to the hook implementation.
 * @return
 *   The return value of the hook implementation.
 */
function video_cck_include_invoke() {
  $args     = func_get_args();
  $provider  = array_shift($args);
  $hook     = array_shift($args);
  $function = 'video_cck_' . $provider . '_' . $hook;
  video_cck_system_list($provider);
  return video_cck_include_hook($provider, $hook) ? call_user_func_array($function, $args) : NULL;
}

function video_cck_embed_form($field, $item, $formatter, $node) {
  $embed = $item['value'];
  $width = $field['widget']['video_width'];
  $height = $field['widget']['video_height'];
  $autoplay = $field['widget']['video_autoplay'];
  $text = video_cck_include_invoke($item['provider'], 'video', $embed, $width, $height, $field, $item, $autoplay);
  $form = array();
  $form['video_cck_embed'] = array(
    '#type' => 'textarea',
    '#title' => t('Embed Code'),
    '#description' => t('To embed this video on your own site, simply copy and paste the html code from this text area.'),
    '#default_value' => $text,
  );
  return $form;
}

function theme_video_cck_embed($field, $item, $formatter, $node) {
  if ($item['value'] && $item['provider']) {
    $output = drupal_get_form('video_cck_embed_form', $field, $item, $formatter, $node);
  }
  return $output;
}

function theme_video_cck_thumbnail($field, $item, $formatter, $node) {
  if ($item['value'] && $item['provider']) {
    $width = $field['widget']['thumbnail_width'] ? $field['widget']['thumbnail_width'] : variable_get('video_cck_default_thumbnail_width', VIDEO_CCK_DEFAULT_THUMBNAIL_WIDTH);
    $height = $field['widget']['thumbnail_height'] ? $field['widget']['thumbnail_height'] : variable_get('video_cck_default_thumbnail_height', VIDEO_CCK_DEFAULT_THUMBNAIL_HEIGHT);
    $thumbnail_url = video_cck_include_invoke($item['provider'], 'thumbnail', $field, $item, $formatter, $node, $width, $height);
    if ($thumbnail_url) {
      $output = l('<img src="' . $thumbnail_url . '" width="' . $width  . '" height="' . $height  . '" alt="' . t('See Video') . '" title="' . t('See Video') . '" />', 'node/' . $node->nid, array(), NULL, NULL, false, true);
    }
    else {
      $output .= l(t('See Video'), 'node/' . $node->nid);
    }
  }
  return $output;
}

function theme_video_cck_video($field, $item, $formatter, $node) {
  if ($item['value'] && $item['provider']) {
    $embed = $item['value'];
    $width = $field['widget']['video_width'] ? $field['widget']['video_width'] : variable_get('video_cck_default_video_width', VIDEO_CCK_DEFAULT_VIDEO_WIDTH);;
    $height = $field['widget']['video_height'] ? $field['widget']['video_height'] : variable_get('video_cck_default_video_height', VIDEO_CCK_DEFAULT_VIDEO_HEIGHT);;
    $autoplay = $field['widget']['video_autoplay'];
    $output = video_cck_include_invoke($item['provider'], 'video', $embed, $width, $height, $field, $item, $autoplay);
  }
  return $output;
}

function theme_video_cck_preview($field, $item, $formatter, $node) {
  if ($item['value'] && $item['provider']) {
    $embed = $item['value'];
    $width = $field['widget']['preview_width'] ? $field['widget']['preview_width'] : variable_get('video_cck_default_preview_width', VIDEO_CCK_DEFAULT_PREVIEW_WIDTH);;
    $height = $field['widget']['preview_height'] ? $field['widget']['preview_height'] : variable_get('video_cck_default_preview_height', VIDEO_CCK_DEFAULT_PREVIEW_HEIGHT);;
    $autoplay = $field['widget']['preview_autoplay'];
    $output = video_cck_include_invoke($item['provider'], 'preview', $embed, $width, $height, $field, $item, $autoplay);
  }
  return $output;
}
