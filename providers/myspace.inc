<?php

define('VIDEO_CCK_MYSPACE_MAIN_URL', 'http://vids.myspace.com/index.cfm?fuseaction=vids.videos');

function video_cck_myspace_info() {
  $name = t('MySpace');
  $features = array(
    array(t('Thumbnails'), t('No'), ''),
    array(t('Autoplay'), t('No'), ''),
  );
  return array(
    'provider' => 'myspace',
    'name' => $name,
    'url' => VIDEO_CCK_MYSPACE_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l($name, VIDEO_CCK_MYSPACE_MAIN_URL, array('target' => '_blank')))),
    'supported_features' => $features,
  );
}

function video_cck_myspace_settings() {
  $form = array();
  return $form;
}

function video_cck_myspace_extract($embed) {
  return array(
    '@src="myspace\.com/index.cfm\?fuseaction=vids\.individual&videoid=([^"]*)"@',
    '@myspace\.com/index\.cfm\?fuseaction=vids\.individual&videoid=(.*)@',
  );
}

function video_cck_myspace_video_link($video_code) {
  return 'http://vids.myspace.com/index.cfm?fuseaction=vids.individual&videoid=' . $video_code;
}

function theme_video_cck_myspace_flash($embed, $width, $height) {
  if ($embed) {
    $output .= '<embed src="http://lads.myspace.com/videos/vplayer.swf" flashvars="m=' . $embed . '&type=video" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '"></embed>';
  }
  return $output;
}

function video_cck_myspace_thumbnail($field, $item, $formatter, $node, $width, $height) {
  return '';
}

function video_cck_myspace_video($embed, $width, $height, $field, $item) {
  $output = theme('video_cck_myspace_flash', $embed, $width, $height);
  return $output;
}

function video_cck_myspace_preview($embed, $width, $height, $field, $item) {
  $output = theme('video_cck_myspace_flash', $embed, $width, $height);
  return $output;
}
